import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "/api": {
        target: "https://data.cdc.gov",
        changeOrigin: true,
      },
      "/echarts": {
        target: "https://echarts.apache.org",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/echarts/, "examples"),
      },
    },
  },
})
