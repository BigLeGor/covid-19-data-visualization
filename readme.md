# working time

- init, from 03/19/2022 19:00 to 03/20/2022 01:05.

- add map chart and line chart, from 03/20/2022 9:00 to 03/20/2022 12:30.

- add deployment related files, and add desc in readme, from 03/20/2022 13:30 to 03/20/2022 14:45.

- deploy site on my raspberrypi, setup frp to open access, from 03/20/2022 19:00 to 03/20/2022 19:45.

- fix some bugs, do some enhancement on UI, and add proxy cache on respberrypi for performance, from 03/20/2022 20:30 to 03/20/2022 21:30.


# Local Dev
- npm i && npm run dev, then you could start local dev

# deploy with docker
1. modify `site-nginx.conf` if you need

2. build the image
    ```
    docker build -t kong-frontend .
    ```

3. start the container, please notice that, you should specify the path of nginx cache first
    ```
    docker run -d \
         --name covid-19-data-visualization \
         -p 80:80 \
         -v /path/of/nginx/cache:/data/nginx/cache \
         kong-frontend
    ```