import { FC, useEffect, useState, useCallback } from 'react';
import { getTableData, getTotalSize, Order } from '../actions';
import { Loading } from '../utils';

const Home: FC = () => {
    const [tableLoading, setTableLoading] = useState<boolean>(false);
    const [tableData, setTableData] = useState<{
        sortBy: string,
        orderBy: Order,
        colName: string[],
        data: any[],
        currPage: number,
        pageSize: number,
        total: number,
    }>({
        sortBy: '',
        orderBy: Order.ASC,
        colName: [],
        data: [],
        currPage: 1,
        pageSize: 100,
        total: 0,
    })

    useEffect(() => {
        (async () => {
            setTableLoading(true);
            let fetcher = [
                getTableData(tableData.currPage, tableData.pageSize, tableData.total, tableData.sortBy, tableData.orderBy),
            ] as Promise<any>[];

            if (tableData.total === 0) {
                fetcher = [
                    ...fetcher,
                    getTotalSize()
                ];
            }

            const [{ colName, ...data }, total = 0] = await Promise.all(fetcher);

            setTableData(prevState => ({
                ...prevState,
                ...data,
                total: total > 0 ? total : prevState.total,
                ...(prevState.colName.length === 0 ? { colName } : {})
            }));
            setTableLoading(false);
        })();
    }, [tableData.currPage, tableData.sortBy, tableData.orderBy]);

    const onTableHeadClick = useCallback((sortByColName) => {
        setTableData(prevState => ({
            ...prevState,
            sortBy: sortByColName,
            orderBy: sortByColName === prevState.sortBy ?
                (prevState.orderBy === Order.ASC ? Order.DESC : Order.ASC)
                : Order.ASC
        }))
    }, []);

    const onPrevPageBtnClick = useCallback(() => {
        tableData.currPage !== 1 && setTableData(prevState => ({
            ...prevState,
            currPage: prevState.currPage - 1
        }))
    }, [tableData.currPage]);

    const onNextPageBtnClick = useCallback(() => {
        tableData.currPage !== Math.round(tableData.total / tableData.pageSize) && setTableData(prevState => ({
            ...prevState,
            currPage: prevState.currPage + 1
        }))
    }, [tableData.currPage]);


    return (
        <>
            {tableLoading && <Loading />}
            <div className='w-full not-prose relative bg-slate-50 rounded-xl overflow-auto dark:bg-slate-800/25'>
                <div className='relative rounded-xl overflow-auto'>
                    <div className='shadow-sm overflow-hidden my-8'>
                        <table className='border-collapse table-fixed w-full text-sm'>
                            <thead>
                                <tr>
                                    {tableData.colName.map(name => (
                                        <th
                                            className='cursor-pointer border-b dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'
                                            onClick={() => onTableHeadClick(name)}
                                        >
                                            {name}
                                            {name === tableData.sortBy ? (tableData.orderBy === Order.ASC ? '👆' : '👇') : ''}
                                        </th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody className='bg-white dark:bg-slate-800'>
                                {tableData.data.map((row: any) => (
                                    <tr>
                                        {tableData.colName.map(name => (
                                            <td className='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>
                                                {row[name] || '-'}
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div className='flex justify-center my-8 mb-0'>
                <nav>
                    <ul className='inline-flex'>
                        <li>
                            <button
                                className={`h-10 px-5 text-indigo-600 transition-colors duration-150 bg-white rounded-l-lg focus:shadow-outline hover:bg-indigo-100 ${tableData.currPage === 1 ? 'opacity-50 cursor-not-allowed' : ''}`}
                                onClick={() => onPrevPageBtnClick()}
                            >
                                Prev
                            </button>
                        </li>
                        <li>
                            <button
                                className={`h-10 px-5 text-indigo-600 transition-colors duration-150 bg-white rounded-r-lg focus:shadow-outline hover:bg-indigo-100 ${tableData.currPage === Math.round(tableData.total / tableData.pageSize) ? 'opacity-50 cursor-not-allowed' : ''}`}
                                onClick={() => onNextPageBtnClick()}
                            >
                                Next
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
        </>
    );
};

export default Home;
