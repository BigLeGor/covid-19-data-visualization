import { useEffect, useState, LegacyRef } from 'react';
import { registerMap } from 'echarts';
import { getConfirmCaseByState, getMapEchartOption, getGeoUSA } from '../actions';
import { useEcharts } from '../utils';

const MapChartView = () => {
  const [loading, setLoading] = useState(false);
  const [mapChartRoot, mapChartInst] = useEcharts();

  useEffect(() => {
    (async () => {
      setLoading(true);
      const usaJson = await getGeoUSA();
      registerMap('USA', usaJson, {
        Alaska: {
          left: -131,
          top: 25,
          width: 15
        },
        Hawaii: {
          left: -110,
          top: 28,
          width: 5
        },
        'Puerto Rico': {
          left: -76,
          top: 26,
          width: 2
        }
      });
      const data = await getConfirmCaseByState();
      const chartOpt = getMapEchartOption(data as any);
      (mapChartInst as any).current?.setOption(chartOpt);
      setLoading(false);
    })();
  }, []);

  useEffect(() => {
    if (loading) {
      (mapChartInst as any).current.showLoading();
    } else {
      (mapChartInst as any).current.hideLoading();
    }
  }, [loading]);

  return (
    <div className='w-full h-screen not-prose relative bg-slate-50 rounded-xl overflow-auto dark:bg-slate-800/25'>
      <div className='relative rounded-xl overflow-hidden h-4/5'>
        <div className='shadow-sm overflow-hidden my-8 h-full'>
          <div className='flex justify-center'>
            <h1>USA Covid-19 Confirm Case</h1>
          </div>
          <div
            ref={mapChartRoot as LegacyRef<HTMLDivElement>}
            className='min-h-full'
          >
            MapChartView
          </div>
        </div>
      </div>
    </div>
  );
};

export default MapChartView