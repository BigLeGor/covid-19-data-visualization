import { useEffect, useState, LegacyRef } from 'react';
import { getConfirmCaseBySubmissionDate, getLineEchartOption } from '../actions';
import { useEcharts } from '../utils';

const LineChartView = () => {
    const [loading, setLoading] = useState(false);
    const [lineChartRoot, lineChartInst] = useEcharts();

    useEffect(() => {
        (async () => {
            setLoading(true);
            const data = await getConfirmCaseBySubmissionDate();
            const chartOpt = getLineEchartOption(data);
            (lineChartInst as any).current?.setOption(chartOpt);
            setLoading(false);
        })();
    }, []);

    useEffect(() => {
        if (loading) {
            (lineChartInst as any).current.showLoading();
        } else {
            (lineChartInst as any).current.hideLoading();
        }
    }, [loading]);

    return (
        <div className='w-full h-screen not-prose relative bg-slate-50 rounded-xl overflow-auto dark:bg-slate-800/25'>
            <div className='relative rounded-xl overflow-hidden h-4/5'>
                <div className='shadow-sm overflow-hidden my-8 h-full'>
                    <div className='flex justify-center'>
                        <h1>USA Covid-19 Confirm Case</h1>
                    </div>
                    <div
                        ref={lineChartRoot as LegacyRef<HTMLDivElement>}
                        className='min-h-full'
                    >
                        LineChartView
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LineChartView;