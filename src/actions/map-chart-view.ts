import { getJson } from "./shared";

const allStatesMapping = Object.freeze({
  AL: "Alabama",
  AK: "Alaska",
  AZ: "Arizona",
  AR: "Arkansas",
  CA: "California",
  CO: "Colorado",
  CT: "Connecticut",
  DE: "Delaware",
  FL: "Florida",
  GA: "Georgia",
  HI: "Hawaii",
  ID: "Idaho",
  IL: "Illinois",
  IN: "Indiana",
  IA: "Iowa",
  KS: "Kansas",
  KY: "Kentucky",
  LA: "Louisiana",
  ME: "Maine",
  MD: "Maryland",
  MA: "Massachusetts",
  MI: "Michigan",
  MN: "Minnesota",
  MS: "Mississippi",
  MO: "Missouri",
  MT: "Montana",
  NE: "Nebraska",
  NV: "Nevada",
  NH: "New hampshire",
  NJ: "New jersey",
  NM: "New mexico",
  NY: "New York",
  NC: "North Carolina",
  ND: "North Dakota",
  OH: "Ohio",
  OK: "Oklahoma",
  OR: "Oregon",
  PA: "Pennsylvania",
  RI: "Rhode island",
  SC: "South carolina",
  SD: "South dakota",
  TN: "Tennessee",
  TX: "Texas",
  UT: "Utah",
  VT: "Vermont",
  VA: "Virginia",
  WA: "Washington",
  WV: "West Virginia",
  WI: "Wisconsin",
  WY: "Wyoming",
});

interface ConfirmCaseBySateRes {
  __dimension_alias__: string;
  __measure_alias__: string;
}

const getConfirmCaseByState = async () => {
  try {
    const queryString =
      "SELECT `state` AS __dimension_alias__, SUM(`conf_cases`) AS __measure_alias__  GROUP BY `state` ORDER BY __measure_alias__ DESC NULL LAST LIMIT 1001";
    const url = `/api/id/9mfq-cb36.json?$query=${encodeURIComponent(
      queryString
    )}&$$read_from_nbe=true&$$version=2.1`;

    const data = (await getJson(url)) as ConfirmCaseBySateRes[];
    return data.map((e) => ({
      name: (allStatesMapping as any)[e.__dimension_alias__],
      value: parseInt(e.__measure_alias__),
    }));
  } catch (ex) {
    console.error(ex);
    return [];
  }
};

const getGeoUSA = async () => {
  try {
    const url = "/echarts/data/asset/geo/USA.json";
    const mapData = await getJson(url);
    return mapData;
  } catch (ex) {
    console.error(ex);
    return {};
  }
};

const getMapEchartOption = (data = []) => {
  const values = data.map(({ value }) => (Number.isNaN(value) ? 0 : value));
  const option = {
    tooltip: {
      trigger: "item",
      showDelay: 0,
      transitionDuration: 0.2,
    },
    visualMap: {
      left: "right",
      min: 500000,
      max: Math.max(...values),
      inRange: {
        color: [
          "#313695",
          "#4575b4",
          "#74add1",
          "#abd9e9",
          "#e0f3f8",
          "#ffffbf",
          "#fee090",
          "#fdae61",
          "#f46d43",
          "#d73027",
          "#a50026",
        ],
      },
      text: ["High", "Low"],
      calculable: true,
    },
    series: [
      {
        name: "USA Covid-19 Confirm Case",
        type: "map",
        roam: true,
        map: "USA",
        emphasis: {
          label: {
            show: true,
          },
        },
        data,
      },
    ],
  };
  return option;
};

export { getConfirmCaseByState, getMapEchartOption, getGeoUSA };
