import { getJson } from './shared';

enum Order {
    ASC = 'asc',
    DESC = 'desc'
}

const getTableData = async (currPage = 1, pageSize = 100, total = 0, sortBy = '', orderBy = Order.ASC) => {
    const queryString = `select *, :id ${sortBy !== '' ? `order by \`${sortBy}\` ${orderBy}` : ''} offset ${(currPage - 1) * pageSize} limit ${pageSize}`;

    try {
        const data = await getJson(`/api/id/8396-v7yb.json?$query=${encodeURIComponent(queryString)}`);
        const [firstElem] = data;
    
        return {
            sortBy,
            colName: Object.keys(firstElem),
            data,
            currPage,
            pageSize,
            total,
        };
    } catch (ex) {
        console.error(ex);
        return {
            colName: [],
            data: [],
            currPage,
            pageSize,
            total,
        };
    }
};

const getTotalSize = async () => {
    try {
        const url = '/api/id/8396-v7yb.json?$query=select%20*%2C%20%3Aid%20%20|%3E%20select%20count(*)%20as%20__count_alias__&$$read_from_nbe=true&$$version=2.1';
        const [{ __count_alias__: total }] = await getJson(url);
        return parseInt(total);
    } catch (ex) {
        console.error(ex);
        return 0;
    }
};


export {
    getTableData,
    getTotalSize,
    Order
};