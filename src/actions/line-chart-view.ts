import { getJson } from "./shared";

interface ConfirmCaseBySubmissionDateRes {
  __dimension_alias__: string;
  __measure_alias__: string;
}

const getConfirmCaseBySubmissionDate = async () => {
  try {
    const queryString =
      "SELECT date_trunc_ym(`submission_date`) AS __dimension_alias__, SUM(`conf_cases`) AS __measure_alias__ WHERE `submission_date` IS NOT NULL AND `submission_date` < '9999-01-01' AND `submission_date` >= '1000-01-01' AND (1=1) GROUP BY date_trunc_ym(`submission_date`) LIMIT 1001";

    const url = `/api/id/9mfq-cb36.json?$query=${encodeURIComponent(
      queryString
    )}&$$read_from_nbe=true&$$version=2.1`;

    const data = (await getJson(url)) as ConfirmCaseBySubmissionDateRes[];
    return data
      .sort(
        (prev, curr) =>
          new Date(prev.__dimension_alias__).getTime() -
          new Date(curr.__dimension_alias__).getTime()
      )
      .map((e) => [
        e.__dimension_alias__.split("T")[0],
        parseInt(e.__measure_alias__),
      ]) as any[];
  } catch (ex) {
    console.error(ex);
    return [];
  }
};

const getLineEchartOption = (data: any[] = []) => {
  const option = {
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "cross",
        crossStyle: {
          color: "#999",
        },
      },
    },
    grid: {
      left: "10%",
      right: "10%",
      bottom: "90px",
      top: "15px",
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      axisPointer: {
        type: "shadow",
      },
    },
    yAxis: {
      type: "value",
      axisLabel: {
        formatter: (value: number) => {
          const million = 1000000;
          const val = value / million;
          if (val > 1000) {
            return `${val / 1000}B`;
          }

          return `${val}M`;
        },
      },
    },
    series: [
      {
        type: "line",
        data,
      },
    ],
  };

  return option;
};

export { getConfirmCaseBySubmissionDate, getLineEchartOption };
