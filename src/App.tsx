import { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { withNavBar, NavLink as Link, Loading } from './utils';

const Home = lazy(() => import('./pages/Home'));
const MapChartView = lazy(() => import('./pages/MapChartView'));
const LineChartView = lazy(() => import('./pages/LineChartView'));

const getNavList = () => [
  <Link className='text-white' to="/">Home</Link>,
  <Link className='text-white' to="/map-chart">MapChartView</Link>,
  <Link className='text-white' to="/line-chart">LineChartView</Link>
];

const App = () => (
  <Router>
    <Suspense fallback={<Loading>This may take a few seconds, please don't close this page.</Loading>}>
      <Routes>
        <Route path='/' element={
          withNavBar(Home)(getNavList())
        } />
        <Route path='/map-chart' element={
          withNavBar(MapChartView)(getNavList())
        } />
        <Route path='/line-chart' element={
          withNavBar(LineChartView)(getNavList())
        } />
      </Routes>
    </Suspense>
  </Router>
);

export default App;
