import { FC, ReactElement, useEffect, useRef } from "react";
import { Link, LinkProps, useLocation } from 'react-router-dom';
import * as echarts from 'echarts';

const withNavBar = (PageComp: FC<any>) => (links: ReactElement[] = []) => (
    <div className='flex flex-col items-center h-screen p-4'>
        <ul className="flex flex-grid:1 mb-4">
            {links}
        </ul>
        <PageComp />
    </div>
);

const NavLink: FC<LinkProps> = ({ className, to, ...restProps }) => {
    const location = useLocation();
    const active = to === location.pathname;

    return (
        <li className='flex-1 mr-2'>
            <Link
                {...restProps}
                to={to}
                className={`text-center block border rounded py-2 px-4 ${active ? 'border-blue-500 bg-blue-500 hover:bg-blue-700 text-white' : 'border-white hover:border-gray-200 text-blue-500 hover:bg-gray-200'} ${className}`}
            />
        </li>
    );
};

const useEcharts = () => {
    const compRoot = useRef<HTMLDivElement>(null);
    const compChartInst = useRef<echarts.ECharts | null>(null);

    useEffect(() => {
        compChartInst.current = echarts.init(compRoot.current as HTMLElement);
        window.addEventListener('resize', () => {
            compChartInst.current?.resize();
        });
    }, []);

    return [compRoot, compChartInst];
};

const Loading: FC = ({ children }) => (
    <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-gray-700 opacity-75 flex flex-col items-center justify-center">
        <div className="loader ease-linear rounded-full border-4 border-t-4 border-gray-200 h-12 w-12 mb-4"></div>
        <h2 className="text-center text-white text-xl font-semibold">Loading...</h2>
        {children && (
            <p className="w-1/3 text-center text-white">{children}</p>
        )}
    </div>
);

export {
    withNavBar,
    NavLink,
    useEcharts,
    Loading
};
